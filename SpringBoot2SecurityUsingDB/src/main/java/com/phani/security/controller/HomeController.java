package com.phani.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@GetMapping("/home")
	public String showHome() {
		return "HomePage";
	}
	
	@GetMapping("/admin")
	public String showAdmin() {
		return "AdminPage";
	}
	
	@GetMapping("/emp")
	public String showEmployee() {
		System.out.println("Inside Employee");
		return "EmployeePage";
	}
	
	@GetMapping("/profile")
	public String showProfile() {
	return "ProfilePage";
	}
	
	@GetMapping("/denied")
	public String accessDeniedPage() {
		return "AccessDeniedPage";
	}

}

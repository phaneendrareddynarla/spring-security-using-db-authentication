package com.phani.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot2SecurityUsingDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot2SecurityUsingDbApplication.class, args);
	}

}
